# Lithe Archive (`.lar`)

Lithe Archive is a custom binary archive file format made for storing compiled code and assets for plugin/mod based programs.

There are two versions of `.lar` archives which can be figured out through their magic bytes.
`LA10` is the first version of the archive based of Quake's `.pak` archive, and was made to be simple to read and write.
`LA11` is the second version which added some new features like: variable length file paths/names and custom bit fields/flags.

## Format

### Version 1.0

Since version `1.0` was based off Quake's `.pak` format, it only changes and removed a few aspects. There is no entry table offset as the list is now right after the header (always 8 bytes from the beginning). And the 56 byte limit for the file name was upped to 120 bytes to allow for more complex file structures.

```lua
-- Header
Version [u32 | 4 bytes] (always 'LA10')
Entry List Size [u32 | 4 bytes]

-- Entry Header List [128 bytes] (repeats {Entry List Size / 128} times)
  -- For Each Entry
    Entry Name [120 bytes] (null terminated)
    Entry Data Size [u32 | 4 bytes]
    Entry Data Offset [u32 | 4 bytes]

File Data [u8 | unknown size]
```

### Version 1.1

This version allows for a dynamically sized entry name, along with an increased character count allowing for 65535 characters, this allows for much longer entry name without using a lot of space.

Flags were added to both the header and entries, these by default are zeroed out as they are custom and will change depending on which program is reading the file and how it will be used.

```lua
-- Header
Version [u32 | 4 bytes] (always 'LA11')
Flags [u32 | 4 bytes] (custom, used by a program)
Entry List Size [u32 | 4 bytes]

-- File Entries
  -- For Each Entry
    Name Length [u16 | 2 bytes]
    Name Data [u8 | length determined by previous 2 bytes]

    Flags [u32 | 4 bytes] (custom, used by a program)

    Size [u32 | 4 bytes]
    Offset [u32 | 4 bytes]

File Data [u8 | unknown size]
```