pub mod de;
pub mod ser;

pub use crate::{de::ArchiveReader, ser::ArchiveWriter};

pub enum ArchiveVersion {
    OneZero,
    OneOne,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Running v1.0");

    one_zero()?;

    println!();

    println!("Running v1.1");

    one_one()?;

    Ok(())
}

fn one_zero() -> Result<(), Box<dyn std::error::Error>> {
    let mut writer = ArchiveWriter::default();

    writer.add_data("folder/test.lthc", "example data", None);
    writer.add_data("folder/test2.lthc", "example data 2", None);

    writer.write(ArchiveVersion::OneZero, "one-zero.lar")?;

    let mut reader = ArchiveReader::read("one-zero.lar")?;

    println!("Entries: {:?}", reader.entries());

    if let Some(data) = reader.get_bytes("folder/test2.lthc")? {
        let text_data = String::from_utf8(data)?;

        println!(r#"Entry "folder/test2.lthc": {}"#, text_data);

        assert_eq!("example data 2".to_string(), text_data);
    }

    if let Some(data) = reader.get_bytes("folder/test.lthc")? {
        let text_data = String::from_utf8(data)?;

        println!(r#"Entry "folder/test.lthc": {}"#, text_data);

        assert_eq!("example data".to_string(), text_data);
    }

    Ok(())
}

fn one_one() -> Result<(), Box<dyn std::error::Error>> {
    let mut writer = ArchiveWriter::default();

    writer.add_data("folder/test.lthc", "example data", None);
    writer.add_data("folder/test2.lthc", "example data 2", None);

    writer.write(ArchiveVersion::OneOne, "one-one.lar")?;

    let mut reader = ArchiveReader::read("one-one.lar")?;

    println!("Entries: {:?}", reader.entries());

    if let Some(data) = reader.get_bytes("folder/test2.lthc")? {
        let text_data = String::from_utf8(data)?;

        println!(r#"Entry "folder/test2.lthc": {}"#, text_data);

        assert_eq!("example data 2".to_string(), text_data);
    }

    if let Some(data) = reader.get_bytes("folder/test.lthc")? {
        let text_data = String::from_utf8(data)?;

        println!(r#"Entry "folder/test.lthc": {}"#, text_data);

        assert_eq!("example data".to_string(), text_data);
    }

    Ok(())
}
